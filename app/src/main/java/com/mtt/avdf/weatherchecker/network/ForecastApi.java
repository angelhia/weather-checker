package com.mtt.avdf.weatherchecker.network;

        import com.mtt.avdf.weatherchecker.models.CityModel;
        import com.mtt.avdf.weatherchecker.models.Forecast;
        import com.mtt.avdf.weatherchecker.models.Model;

        import retrofit.Call;
        import retrofit.http.GET;
        import retrofit.http.Path;
        import retrofit.http.Query;

/**
 * Created by Gel on 3/27/2016.
 */
public interface ForecastApi{

    @GET("data/2.5/forecast/daily?&appid=7c88f19b4a189fefbffbecd0004b1b1b&cnt=5&units=metric")
    Call<Forecast> getWeatherReport (@Query("q") String city);

    @GET("/data/2.5/weather?&appid=7c88f19b4a189fefbffbecd0004b1b1b&units=metric")
    Call<Model> getWeatherReportToday (@Query("q") String city);

    @GET("/data/2.5/find?&appid=7c88f19b4a189fefbffbecd0004b1b1b&units=metric")
    Call<CityModel> findCity (@Query("q") String city);


}

