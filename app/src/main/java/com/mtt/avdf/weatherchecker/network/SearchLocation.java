package com.mtt.avdf.weatherchecker.network;

import android.content.Context;

import com.mtt.avdf.weatherchecker.R;
import com.mtt.avdf.weatherchecker.listeners.LocationInterfaceListener;
import com.mtt.avdf.weatherchecker.models.CityModel;
import com.mtt.avdf.weatherchecker.utils.GlobalVariables;
import com.squareup.okhttp.OkHttpClient;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Gel on 3/29/2016.
 */
public class SearchLocation {
    private static Context c;
    private static LocationInterfaceListener callback;
    private static String query;

    public SearchLocation(Context _c, LocationInterfaceListener _callback, String _query) {
        this.c = _c;
        this.callback = _callback;
        this.query = _query;
    }

    public static void searchLocation(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GlobalVariables.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();

        ForecastApi service = retrofit.create(ForecastApi.class);


        Call<CityModel> call = service.findCity(query);

        call.enqueue(new Callback<CityModel>() {
            @Override
            public void onResponse(Response<CityModel> response, Retrofit retrofit) {

                try {
                    callback.onReceiveSearchLocationSuccessResult(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onReceiveSearchLocationFailResult(c.getString(R.string.error_result));
            }
        });
    }
}

