package com.mtt.avdf.weatherchecker.models;

/**
 * Created by Gel on 3/25/2016.
 */
public class WeatherResponse {
    private int cod;
    private String base;
    private Weather main;

    public int getCod() {
        return cod;
    }

    public String getBase() {
        return base;
    }

    public Weather getMain() {
        return main;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public void setMain(Weather main) {
        this.main = main;
    }
}
