package com.mtt.avdf.weatherchecker.models;

import android.app.ListFragment;

import java.io.Serializable;

/**
 * Created by Gel on 3/28/2016.
 */
public class CityModel implements Serializable
{

    public CityModel(String message,String count,String cod,CityModelList[] list){
        this.message = message;
        this.count = count;
        this.cod = cod;
        this.list = list;
    }


    private String message;

    private String count;

    private String cod;

    private CityModelList[] list;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCount ()
    {
        return count;
    }

    public void setCount (String count)
    {
        this.count = count;
    }

    public String getCod ()
    {
        return cod;
    }

    public void setCod (String cod)
    {
        this.cod = cod;
    }

    public CityModelList[] getList ()
    {
        return list;
    }

    public void setList (CityModelList[] list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", count = "+count+", cod = "+cod+", list = "+list+"]";
    }
}