package com.mtt.avdf.weatherchecker.utils;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.mtt.avdf.weatherchecker.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Gel on 3/28/2016.
 */
public class Helper {
    private static Dialog dg;

    private static boolean isDialogShowing = false;

    public static void showProcessingDialog(Context c) {
        dg = new Dialog(c, R.style.ProgressDialogTheme);
        dg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dg.setContentView(R.layout.loading);
        dg.show();
        Log.d("helper", "show dialog");
        isDialogShowing = true;
    }

    public static void dismissDialog() {
        if (isDialogShowing) {
            dg.dismiss();
            Log.d("helper", "dismiss dialog");
            isDialogShowing = false;
        }
    }

    public static String formatTempToCelcius(String temp){
        return temp + " \u2103";
    }

    public static String formatDate(String epochString) {
        long epoch = Long.parseLong(epochString);
        Date date = new Date(epoch * 1000);
        Log.d("DATE", epochString);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return dateFormat.format(date);
    }

    public static void hideKeyboard(AppCompatActivity a){
        a.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static String formatDate(final long unixTimestamp, Context c) {
        long MILLISECONDS_IN_SECONDS = 1000;
        final long milliseconds = unixTimestamp * MILLISECONDS_IN_SECONDS;
        String day;

        if (isToday(milliseconds)) {
            day = c.getResources().getString(R.string.today);
        } else if (isTomorrow(milliseconds)) {
            day = c.getResources().getString(R.string.tomorrow);
        } else {
            day = getDayOfWeek(milliseconds);
        }
        return day;
    }

    private static String getDayOfWeek(final long milliseconds) {
        return new SimpleDateFormat("EEEE").format(new Date(milliseconds));
    }

    private static boolean isToday(final long milliseconds) {
        final SimpleDateFormat dayInYearFormat = new SimpleDateFormat("yyyyD");
        final String nowHash = dayInYearFormat.format(new Date());
        final String comparisonHash = dayInYearFormat.format(new Date(milliseconds));
        return nowHash.equals(comparisonHash);
    }

    private static boolean isTomorrow(final long milliseconds) {
        final SimpleDateFormat dayInYearFormat = new SimpleDateFormat("yyyyD");
        final int tomorrowHash = Integer.parseInt(dayInYearFormat.format(new Date())) + 1;
        final int comparisonHash = Integer.parseInt(dayInYearFormat.format(new Date(milliseconds)));
        return comparisonHash == tomorrowHash;
    }

    public static int getIcon(String icon){
        int iconBg = R.drawable.d01;
        switch (icon) {
            case "01d":
                iconBg = R.drawable.d01;
                break;
            case "02d":
                iconBg = R.drawable.d02;
                break;
            case "03d":
                iconBg = R.drawable.d03;
                break;
            case "04d":
                iconBg = R.drawable.d04;
                break;
            case "09d":
                iconBg = R.drawable.d09;
                break;
            case "10d":
                iconBg = R.drawable.d10;
                break;
            case "11d":
                iconBg = R.drawable.d11;
                break;
            case "13d":
                iconBg = R.drawable.d13;
                break;
            case "50d":
                iconBg = R.drawable.d50;
                break;
            case "01n":
                iconBg = R.drawable.n01;
                break;
            case "02n":
                iconBg = R.drawable.n02;
                break;
            case "03n":
                iconBg = R.drawable.n03;
                break;
            case "04n":
                iconBg = R.drawable.n04;
                break;
            case "09n":
                iconBg = R.drawable.n09;
                break;
            case "10n":
                iconBg = R.drawable.n10;
                break;
            case "11n":
                iconBg = R.drawable.n11;
                break;
            case "13n":
                iconBg = R.drawable.n13;
                break;
            case "50n":
                iconBg = R.drawable.n50;
                break;
            default:
                iconBg = R.drawable.d01;
                break;
        }

        return iconBg;
    }

}