package com.mtt.avdf.weatherchecker.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.mtt.avdf.weatherchecker.R;
import com.mtt.avdf.weatherchecker.models.CityModelList;

/**
 * Created by Gel on 3/28/16.
 * *
 */
public class CountryListAdapter extends BaseAdapter {


    Context context;
    CityModelList[] newList;
    private LayoutInflater inflater;
    public CountryListAdapter(Context context,CityModelList[] newList){
        this.context = context;
        this.newList = newList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return newList.length;
    }

    @Override
    public CityModelList getItem(int position) {
        return newList[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.country_item, null);
        }
        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
        String country = getItem(position).getSys().getCountry();
        String name = getItem(position).getName();
        txtName.setText(name+", "+country);
        return convertView;
    }

}
