package com.mtt.avdf.weatherchecker.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuInflater;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.mtt.avdf.weatherchecker.R;
import com.mtt.avdf.weatherchecker.fragments.SearchResultFragment;
import com.mtt.avdf.weatherchecker.fragments.TodayForecastFragment;
import com.mtt.avdf.weatherchecker.listeners.LocationInterfaceListener;
import com.mtt.avdf.weatherchecker.models.CityModel;
import com.mtt.avdf.weatherchecker.network.SearchLocation;
import com.mtt.avdf.weatherchecker.utils.GlobalVariables;
import com.mtt.avdf.weatherchecker.utils.Helper;;

import retrofit.Response;

public class MainActivity extends AppCompatActivity
        implements SearchView.OnQueryTextListener, SearchView.OnCloseListener, LocationInterfaceListener, TodayForecastFragment.OnDataPass {

    private TodayForecastFragment todayForecastFragment;
    private static String LOG_TAG = "---MainActivity---";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);

        initFragments();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d(LOG_TAG, "Search query: "+query);
        Helper.showProcessingDialog(MainActivity.this);
        Helper.hideKeyboard(MainActivity.this);
        search(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d(LOG_TAG, "Search new text: "+newText);
        return false;
    }

    @Override
    public boolean onClose() {
        return false;
    }

    private void initFragments(){
        todayForecastFragment = TodayForecastFragment.newInstance(GlobalVariables.DEFAULT_LOCATION);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.content_frame, todayForecastFragment, GlobalVariables.FRAG_TODAY_FORECAST).commit();
    }

    private void search(String query){
        SearchLocation searchLocation = new SearchLocation(MainActivity.this, MainActivity.this, query);
        searchLocation.searchLocation();
    }

    private void openSearchResult(CityModel model){
        Fragment fragment = SearchResultFragment.newInstance(model);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.content_frame, fragment, GlobalVariables.FRAG_SEARCH_RESULT).commit();
    }

    @Override
    public void onReceiveSearchLocationSuccessResult(Response<CityModel> response) {
        Helper.dismissDialog();
        openSearchResult(response.body());
    }

    @Override
    public void onReceiveSearchLocationFailResult(String errorMessage) {
        Helper.dismissDialog();
        Snackbar.make(findViewById(android.R.id.content), errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onDataPassListener(String data) {
        getSupportActionBar().setTitle(data.toUpperCase());
    }
}