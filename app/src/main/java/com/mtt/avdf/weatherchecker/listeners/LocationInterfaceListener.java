package com.mtt.avdf.weatherchecker.listeners;

import com.mtt.avdf.weatherchecker.models.CityModel;
import com.mtt.avdf.weatherchecker.models.Model;

import retrofit.Response;

/**
 * Created by Gel on 3/29/2016.
 */
public interface LocationInterfaceListener {

    public void onReceiveSearchLocationSuccessResult(Response<CityModel> response);
    public void onReceiveSearchLocationFailResult(String errorMessage);
}
