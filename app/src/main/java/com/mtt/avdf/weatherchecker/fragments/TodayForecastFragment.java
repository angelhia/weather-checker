package com.mtt.avdf.weatherchecker.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mtt.avdf.weatherchecker.R;
import com.mtt.avdf.weatherchecker.adapters.WeatherForecastAdapter;
import com.mtt.avdf.weatherchecker.listeners.WeatherForecastInterfaceListener;
import com.mtt.avdf.weatherchecker.models.Forecast;
import com.mtt.avdf.weatherchecker.models.ListForecast;
import com.mtt.avdf.weatherchecker.models.Model;
import com.mtt.avdf.weatherchecker.network.GetDailyWeatherForecast;
import com.mtt.avdf.weatherchecker.network.GetWeatherForecast;
import com.mtt.avdf.weatherchecker.utils.GlobalVariables;
import com.mtt.avdf.weatherchecker.utils.Helper;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;
import retrofit.Response;


public class TodayForecastFragment extends Fragment implements  WeatherForecastInterfaceListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String LOCATION_QUERY = "locationQuery";

    // TODO: Rename and change types of parameters
    private String locationQuery="";

    private TextView todayDate, todayTemp, todayTempHgh, todayTempLow, todayDesc, todayPressure, todayHumidity;
    private ImageView todayIcon;

    private ListView mForecastListView;

    private LineChartView chart;

    private static String LOG_TAG = "---TodayForecastFragment---";


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param locationQuery Parameter 1.
     * @return A new instance of fragment TodayForecastFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TodayForecastFragment newInstance(String locationQuery) {
        TodayForecastFragment fragment = new TodayForecastFragment();
        Bundle args = new Bundle();
        args.putString(LOCATION_QUERY, locationQuery);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            locationQuery = getArguments().getString(LOCATION_QUERY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_today_forecast, container, false);
        todayTemp = (TextView) rootView.findViewById(R.id.today_temp);
        todayDesc = (TextView) rootView.findViewById(R.id.today_desc);
        todayDate = (TextView) rootView.findViewById(R.id.today_date);
        todayTempHgh = (TextView) rootView.findViewById(R.id.today_high_temp);
        todayTempLow = (TextView) rootView.findViewById(R.id.today_low_temp);
        todayPressure = (TextView) rootView.findViewById(R.id.today_pressure);
        todayHumidity = (TextView) rootView.findViewById(R.id.today_humidity);
        todayIcon = (ImageView) rootView.findViewById(R.id.today_icon);

        mForecastListView = (ListView) rootView.findViewById(R.id.weather_forecast_list);

        chart = (LineChartView) rootView.findViewById(R.id.chart);

        if(TextUtils.isEmpty(locationQuery)){
            locationQuery = GlobalVariables.DEFAULT_LOCATION;
        }

        //send location to Main Activity
        _onDataPassCallback.onDataPassListener(locationQuery);

        getReport(locationQuery);
        get5dDaysReport(locationQuery);
        Helper.showProcessingDialog(getActivity());

        return rootView;
    }

    private void getReport(String query) {
        Log.d(LOG_TAG, "get report: "+query);
        GetWeatherForecast getWeatherForecast = new GetWeatherForecast(getActivity(), this, query);
        getWeatherForecast.getWeatherForeCast();
    }


    private void get5dDaysReport(String query){
        Log.d(LOG_TAG, "get 5 days report: "+query);;
        GetDailyWeatherForecast getDailyWeatherForecast = new GetDailyWeatherForecast(getActivity(), this, query);
        getDailyWeatherForecast.getDailyWeatherForecast();
    }

    @Override
    public void onReceiveWeatherTodaySuccessResult(Response <Model> response) {
        try {
            Helper.dismissDialog();

            //parse response
            String temp = response.body().getMain().getTemp().toString();
            String tempHi = response.body().getMain().getTempMax().toString();
            String tempLow = response.body().getMain().getTempMin().toString();
            String humidity = response.body().getMain().getHumidity().toString();
            String pressure = response.body().getMain().getPressure().toString();
            String desc = response.body().getWeather().get(0).getDescription().toString();
            String date = response.body().getDt().toString();
            String icon = response.body().getWeather().get(0).getIcon().toString();

            todayTemp.setText(Helper.formatTempToCelcius(temp));
            todayTempHgh.setText(Helper.formatTempToCelcius("H: "+tempHi));
            todayTempLow.setText(Helper.formatTempToCelcius("L: " + tempLow));
            todayDesc.setText(desc.toUpperCase());
            todayDate.setText(Helper.formatDate(date));

            todayPressure.setText("P: " + pressure+"Pa");
            todayHumidity.setText("Humidity: " + humidity + "%");

            todayIcon.setImageResource(Helper.getIcon(icon));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReceiveWeatherTodayFailResult(String errorMessage) {
        Helper.dismissDialog();
        Snackbar.make(getActivity().findViewById(android.R.id.content), errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onReceiveWeatherDailySuccessResult(Response<Forecast> response) {
        Helper.dismissDialog();
        ListForecast[] newList = response.body().getList();
        setLineData(newList);
        WeatherForecastAdapter adapter = new WeatherForecastAdapter(getActivity(), newList);
        mForecastListView.setAdapter(adapter);
    }

    @Override
    public void onReceiveWeatherDailyFailResult(String errorMessage) {
        Helper.dismissDialog();
        Snackbar.make(getActivity().findViewById(android.R.id.content), errorMessage, Snackbar.LENGTH_LONG).show();
    }

    private void setLineData(ListForecast [] newList){

        /** line graph will consist of:
         * x-axis = day and
         * y-axis = temperature in degree celsius
         */
        String temp [] = new String[newList.length];
        String label [] = new String[newList.length];
        for (int i = 0; i<newList.length; i++){
            temp [i] = newList[i].getTemp().getDay().toString();
            label [i] = Helper.formatDate(Long.valueOf(newList[i].getDt()), getActivity());
        }

        //set temperature in degree celsius as label of each point
        List<PointValue> values = new ArrayList<PointValue>();
        for (int i = 0; i< temp.length; i++){
            values.add(new PointValue(i, Float.valueOf(temp[i])).setLabel(temp[i]));
        }

        Line line = new Line(values).setColor(Color.BLUE).setCubic(true).setFilled(true).setHasLabels(true);
        List<Line> lines = new ArrayList<Line>();
        lines.add(line);
        LineChartData data = new LineChartData();
        data.setLines(lines);

        //x-axis labels = day of the week
        Axis axisX = new Axis().setHasLines(true);
        List<AxisValue> axisValues = new ArrayList<AxisValue>();
        for (int i = 0; i < 5; i ++) {
            axisValues.add(new AxisValue(i).setLabel(label[i]));
        }
        axisX.setHasTiltedLabels(true).setValues(axisValues).setTextColor(Color.BLACK);

        //y-axis labels = temperature in degree celsius
        Axis axisY = new Axis().setHasLines(true).setTextColor(Color.TRANSPARENT);

        //set x-xis and y-axis
        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisY);

        //finally add line data to chart
        chart.setVisibility(View.VISIBLE);
        chart.setLineChartData(data);
    }

    OnDataPass _onDataPassCallback;

    public interface OnDataPass {
        public void onDataPassListener(String data);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _onDataPassCallback = (OnDataPass) context;
    }
}
