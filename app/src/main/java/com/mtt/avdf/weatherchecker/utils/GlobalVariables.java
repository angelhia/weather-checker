package com.mtt.avdf.weatherchecker.utils;

/**
 * Created by Gel on 3/25/2016.
 */
public class GlobalVariables {

    public static String BASE_URL = "http://api.openweathermap.org";

    public static String FRAG_TODAY_FORECAST = "FRAG_HOME";
    public static String FRAG_SEARCH_RESULT = "FRAG_SEARCH_RESULT";

    public static final String DEFAULT_LOCATION="Dublin, Ireland";

}
