package com.mtt.avdf.weatherchecker.listeners;


import com.mtt.avdf.weatherchecker.models.Forecast;
import com.mtt.avdf.weatherchecker.models.ListForecast;
import com.mtt.avdf.weatherchecker.models.Model;

import retrofit.Response;

/**
 * Created by Gel on 3/29/2016.
 */
public interface WeatherForecastInterfaceListener {

    public void onReceiveWeatherTodaySuccessResult(Response<Model> response);
    public void onReceiveWeatherTodayFailResult (String errorMessage);
    public void onReceiveWeatherDailySuccessResult(Response<Forecast> response);
    public void onReceiveWeatherDailyFailResult(String errorMessage);
}
