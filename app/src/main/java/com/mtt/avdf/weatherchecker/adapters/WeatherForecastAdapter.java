package com.mtt.avdf.weatherchecker.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mtt.avdf.weatherchecker.R;
import com.mtt.avdf.weatherchecker.models.ListForecast;
import com.mtt.avdf.weatherchecker.utils.Helper;

/**
 * Created by Gel on 3/28/2016.
 */
public class WeatherForecastAdapter extends BaseAdapter {

    private Context context;
    private ListForecast [] listForecast;
    private LayoutInflater inflater;

    public WeatherForecastAdapter (Context _context, ListForecast _listForecast []){
        this.context = _context;
        this.listForecast = _listForecast;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listForecast.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.weather_forecast_list_item, null);
        }

        TextView weekDate = (TextView) convertView.findViewById(R.id.week_date);
        TextView weekDesc = (TextView) convertView.findViewById(R.id.week_desc);
        TextView weekTemp = (TextView) convertView.findViewById(R.id.week_temp);
        ImageView weekIcon = (ImageView) convertView.findViewById(R.id.week_icon);

        weekDate.setText(Helper.formatDate(Long.valueOf(listForecast[i].getDt()), context));
        weekDesc.setText(listForecast[i].getWeather()[0].getDescription());
        weekTemp.setText(Helper.formatTempToCelcius(listForecast[i].getTemp().getDay()));
        weekIcon.setImageResource(Helper.getIcon(listForecast[i].getWeather()[0].getIcon()));

        return convertView;
    }
}
