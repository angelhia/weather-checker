package com.mtt.avdf.weatherchecker.network;

import android.content.Context;

import com.mtt.avdf.weatherchecker.R;
import com.mtt.avdf.weatherchecker.listeners.WeatherForecastInterfaceListener;
import com.mtt.avdf.weatherchecker.models.Forecast;
import com.mtt.avdf.weatherchecker.utils.GlobalVariables;
import com.squareup.okhttp.OkHttpClient;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Gel on 3/29/2016.
 */
public class GetDailyWeatherForecast {
    private static Context c;
    private static WeatherForecastInterfaceListener callback;
    private static String query;

    public GetDailyWeatherForecast(Context _c, WeatherForecastInterfaceListener _callback, String _query) {
        this.c = _c;
        this.callback = _callback;
        this.query = _query;
    }

    public static void getDailyWeatherForecast(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GlobalVariables.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();

        ForecastApi service = retrofit.create(ForecastApi.class);


        Call<Forecast> call = service.getWeatherReport(query);

        call.enqueue(new Callback<Forecast>() {
            @Override
            public void onResponse(Response<Forecast> response, Retrofit retrofit) {

                try {
                    callback.onReceiveWeatherDailySuccessResult(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onReceiveWeatherDailyFailResult(c.getString(R.string.error_result));
            }
        });
    }
}

