package com.mtt.avdf.weatherchecker.network;

import android.content.Context;
import android.util.Log;

import com.mtt.avdf.weatherchecker.R;
import com.mtt.avdf.weatherchecker.listeners.WeatherForecastInterfaceListener;
import com.mtt.avdf.weatherchecker.models.Model;
import com.mtt.avdf.weatherchecker.utils.GlobalVariables;
import com.squareup.okhttp.OkHttpClient;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Gel on 3/29/2016.
 */
public class GetWeatherForecast {
    private static Context c;
    private static WeatherForecastInterfaceListener callback;
    private static String query;

   public GetWeatherForecast(Context _c, WeatherForecastInterfaceListener _callback, String _query) {
       this.c = _c;
       this.callback = _callback;
       this.query = _query;
   }

    public static void getWeatherForeCast(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GlobalVariables.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();

        ForecastApi service = retrofit.create(ForecastApi.class);


        Call<Model> call = service.getWeatherReportToday(query);

        call.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Response<Model> response, Retrofit retrofit) {

                try {
                    callback.onReceiveWeatherTodaySuccessResult(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onReceiveWeatherTodayFailResult(c.getString(R.string.error_result));
            }
        });
    }
}

